import unittest
from data_structures.datacenter import Datacenter
import json
import logging

with open(r'responseFile.json') as json_file:
    data = json.load(json_file)

if not data:
    raise ValueError('No data to process')

datacenters = [Datacenter(key, value)
               for key, value in data.items()]



class TestCalc(unittest.TestCase):

    def setUp(self):
        for i in datacenters:
            i.remove_invalid_clusters()

        for i in datacenters:
            for j in i.clusters:
                for k in j.networks:
                    k.remove_invalid_records()
                    k.sort_records()

        self.datacentersCopy = datacenters.copy()
        self.c_clusters_list1 = ['BER-203', 'BER-1']
        self.c_networks_list1 = ["192.168.0.1", "192.168.0.2", "192.168.0.3", "192.168.0.4"]

        self.clustersCopy = []
        for c in self.datacentersCopy[0].clusters:
            self.clustersCopy.append(c.name)

        self.networksCopy = []
        for n in self.datacentersCopy[0].clusters[0].networks[0].entries:
            self.networksCopy.append(n.address)
        pass

    #more tests cases could be added
    def test_remove_invalid_clusters(self):
        self.assertCountEqual(self.clustersCopy, self.c_clusters_list1)


    def test_remove_invalid_records(self):
        self.assertCountEqual(self.networksCopy, self.c_networks_list1)


    def test_sort_records(self):
        self.assertEqual(self.networksCopy, self.c_networks_list1)




if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCalc)
    results = unittest.TextTestRunner(verbosity=2).run(suite)
    unittest.main()