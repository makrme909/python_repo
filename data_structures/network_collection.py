from data_structures.entry import Entry
from netaddr import IPNetwork, IPAddress
import ipaddress

class NetworkCollection:
    def __init__(self, ipv4_network, raw_entry_list):
        self.name = ipv4_network
        self.entries = []
        for value in raw_entry_list:
            self.entries.append(Entry(value['address'], value['available'], value['last_used']))
        """
        Constructor for NetworkCollection data structure.

        self.ipv4_network -> ipaddress.IPv4Network
        self.entries -> list(Entry)
        """

        pass

    def remove_invalid_records(self):
        ok = True
        network = str(self.name)
        a = self.entries

        # if is valid adress
        for i in list(a):
            ok = True
            ipCheck = str(i.address)
            try:
                if ipaddress.ip_address(ipCheck):
                    ok = True
            except:
                ok = False

            ## dif is subadress of parrentNet
            if ok == True:
                if IPAddress(ipCheck) in IPNetwork(network):
                    ok = True
                else:
                    ok = False

            if ok == False:
                a.remove(i)
        """
        Removes invalid objects from the entries list.
        """

    def sort_records(self):
        """
        Sorts the list of associated entries in ascending order.
        DO NOT change this method, make the changes in entry.py :)
        """
        self.entries = sorted(self.entries)
