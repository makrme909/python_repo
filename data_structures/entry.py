import ipaddress

class Entry:
    def __init__(self, address, available, last_used):
        self.address = address
        self.available = available
        self.last_used = last_used

        """
        Constructor for Entry data structure.

        self.address -> str
        self.available -> bool
        self.last_used -> datetime
        """
     #overloaded overload less than operator
    def __lt__(self, other):
        adr1 = ipaddress.IPv4Address(self.address)
        adr2 = ipaddress.IPv4Address(other.address)
        return adr1 < adr2

        pass
