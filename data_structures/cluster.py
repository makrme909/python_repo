from data_structures.network_collection import NetworkCollection


class Cluster:
    def __init__(self, name, network_dict, security_level):
        self.name = name
        self.networks = []
        self.security_level = security_level

        for key, value in network_dict.items():
            self.networks.append(NetworkCollection(key, value))
        """
        Constructor for Cluster data structure.

        self.name -> str
        self.security_level -> int
        self.networks -> list(NetworkCollection)
        """

