from data_structures.cluster import Cluster
import re



class Datacenter:
    def __init__(self, name, cluster_dict):
        self.name = name
        self.clusters = []

        for key, value in cluster_dict.items():
            self.clusters.append(Cluster(key, value['networks'], value['security_level']))
        """
        Constructor for Datacenter data structure.

        self.name -> str
        self.clusters -> list(Cluster)
        """



    def remove_invalid_clusters(self):
        """
        Removes invalid objects from the clusters list.
        """
        parrentNet = str(self.name)
        first_3_l = parrentNet[:3]
        first_3_l = first_3_l.upper()


        filtered = self.clusters

        #full = ['BER-1', 'BER-203', 'BER-4000', 'TEST-1']
        regex = re.compile(
            rf"(?!{first_3_l}-\d{{1,3}}\b)")

        for i in list(filtered):
            if regex.match(str(i.name)):
                filtered.remove(i)
        return filtered
