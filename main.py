from data_structures.datacenter import Datacenter
import requests, json
import logging
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry




URL = "http://www.mocky.io/v2/5e539b332e00007c002dacbe"


def get_data(url, max_retries=5, delay_between_retries=1):
    """
    Fetch the data from http://www.mocky.io/v2/5e539b332e00007c002dacbe
    and return it as a JSON object.
​
    Args:
        url (str): The url to be fetched.
        max_retries (int): Number of retries.
        delay_between_retries (int): Delay between retries in seconds.
    Returns:
        data (dict)
    """
    retry_strategy = Retry(
        total=max_retries,
        backoff_factor=delay_between_retries,
        status_forcelist=[429, 500, 502, 503, 504],
        method_whitelist=["HEAD", "GET", "OPTIONS"]
    )
    adapter = HTTPAdapter(max_retries=retry_strategy)
    session = requests.Session()

    logging.basicConfig(level=logging.DEBUG)

    session.mount("https://", adapter)
    session.mount("http://", adapter)

    response = session.get(url=url)
    data = response.json()

    ### for unittests, the responseFile needs to be copied to tests folder
    json_object = json.dumps(data, indent=2)
    with open('responseFile.json', 'w') as outfile:
        outfile.write(json_object)
        outfile.close()

    return data
    # the rest of your logic here


def main():
    """
    Main entry to our program.
    """

    data = get_data(URL)

    if not data:
        raise ValueError('No data to process')

    datacenters = [
        Datacenter(key, value)
        for key, value in data.items()
    ]

    # suite = unittest.TestLoader().loadTestsFromTestCase(TestCalc)
    # results = unittest.TextTestRunner(verbosity=2).run(suite)



if __name__ == '__main__':
    main()
